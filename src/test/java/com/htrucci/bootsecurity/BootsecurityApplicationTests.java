package com.htrucci.bootsecurity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BootsecurityApplicationTests {

	@Test
	public void testOAuthService(){
		ResourceOwnerPasswordResourceDetails resourceDetails = new ResourceOwnerPasswordResourceDetails();

		resourceDetails.setUsername("user");
		resourceDetails.setPassword("1234");
		resourceDetails.setAccessTokenUri("http://localhost:8080/oauth/token");
		resourceDetails.setClientId("trustedclient");
		resourceDetails.setClientSecret("trustedclient1234");
		resourceDetails.setGrantType("password");
		resourceDetails.setScope(Arrays.asList(new String[] {"read","write","trust"}));
		DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
		OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails, clientContext);

//		Greet greet = restTemplate.getForObject("http://localhost:8080", Greet.class);
//		Assert.assertEquals("HelloWorld!", greet.getMessage());
	}
}
